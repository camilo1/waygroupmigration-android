package com.meteosoft.waygroup.database;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class ImageInfoHelper {
	public void setRefreshDao(Context context, ImageInfo img)
			throws SQLException {
		DataBaseHelper helperDB = new DataBaseHelper(context);
		Dao<ImageInfo, String> ImageDao = helperDB.getImageInfoDao();
		try {
			ImageDao.refresh(img);
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		helperDB.close();
	}

	public void addData(Context context, ImageInfo img) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<ImageInfo, String> dao;
		try {

			dao = dbHelper.getImageInfoDao();
			dao.create(img);
			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<ImageInfo> getImageInfo(Context context, String userType,
			boolean isUploaded) throws SQLException {
		DataBaseHelper helperDB = new DataBaseHelper(context);
		Dao<ImageInfo, String> Dao = helperDB.getImageInfoDao();
		List<ImageInfo> list = null;
		try {
//			QueryBuilder<ImageInfo, String> queryBuilder = Dao.queryBuilder();
//			Where<ImageInfo, String> where = queryBuilder.where();
//			where.eq("userType", userType);
//			where.and();
//			where.eq("isUploaded", isUploaded);
			list = Dao.queryForAll();
			Collections.reverse(list);

		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		helperDB.close();
		return list;
	}

	public void updateData(Context context, ImageInfo image) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<ImageInfo, String> dao;
		try {
			dao = dbHelper.getImageInfoDao();
			dao.update(image);
			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
