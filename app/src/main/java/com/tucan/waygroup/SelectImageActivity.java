package com.tucan.waygroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.meteosoft.infrastructure.Utils;
import com.meteosoft.waygroup.database.ImageInfo;
import com.meteosoft.waygroup.database.ImageInfoHelper;
import com.meteosoft.waygroup.database.ImageInfoUpdate;
import com.meteosoft.waygroup.database.ImageInfoUpdateHelper;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class SelectImageActivity extends Activity {
	private static final int SELECT_PICTURE = 1;

	String selectedImagePath;
	PopupWindow pwindo;
	ImageView img;
	byte[] image = null;
	int userId = 0;
	AddStringTask addStringTask;
	ProgressDialog progress;
	String myLocation;
	String owner;
	String userid;
	String userName;
	String senderName = "senderName";
	EditText description;
	EditText titleEditText;
	NetworkInfo ni;
	File output;
	Date date;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_image);
		progress = new ProgressDialog(SelectImageActivity.this);
		Intent intent1 = getIntent();
		userName = intent1.getStringExtra("UserName");
		userid = intent1.getStringExtra("userType");
		owner = intent1.getStringExtra("owner");
		SharedPreferences preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_PRIVATE);
		myLocation = preference.getString("MyLocation", " ");
		if (userid.equals("1") == true) {
			userId = 1;
		} else if (userid.equals("2") == true) {
			userId = 2;
		}
		Log.v("Userid", " " + userid);
		Log.v("UserId", " " + userName);

		ImageButton cameraBtn = (ImageButton) findViewById(R.id.cameraBtn);
		ImageButton galleryBtn = (ImageButton) findViewById(R.id.galleryBtn);
		ImageButton sendImage = (ImageButton) findViewById(R.id.sendImage);
		ImageButton home_btn = (ImageButton) findViewById(R.id.home1);
		ImageButton webLink1 = (ImageButton) findViewById(R.id.webLink1);
		webLink1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SelectImageActivity.this,
						WebPageActivity.class);
				startActivity(intent);
			}

		});
		home_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}

		});
		cameraBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Intent intent = new
				// Intent("android.media.action.IMAGE_CAPTURE");
				// startActivityForResult(intent, 2);
				// Intent cameraIntent = new
				// Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				// File dir =
				// Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
				// output = new File(dir, "camerascript.png");
				// cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(output));
				// startActivityForResult(cameraIntent, 2);
				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
				/* create instance of File with name img.jpg */
				output = new File(Environment.getExternalStorageDirectory()
						+ File.separator + "img.jpg");
				/* put uri as extra in intent object */
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
				startActivityForResult(intent, 2);
			}
		});
		galleryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"),
						SELECT_PICTURE);
			}
		});
		description = (EditText) findViewById(R.id.description);
		description.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageView editText = (ImageView) findViewById(R.id.editText);
				editText.setVisibility(View.GONE);
				return false;
			}
		});

		titleEditText = (EditText) findViewById(R.id.titleEditText);
		titleEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ImageView imageView = (ImageView) findViewById(R.id.titleImageView);
				imageView.setVisibility(View.GONE);
				return false;
			}
		});

		sendImage.setOnClickListener(new OnClickListener() {
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				ni = cm.getActiveNetworkInfo();

				if (image == null
						|| description.getText().toString().isEmpty() == true
						|| titleEditText.getText().toString().isEmpty() == true) {
					Toast.makeText(getApplicationContext(),
							"Campo Obligatorio vacío", Toast.LENGTH_LONG)
							.show();
				} else if (myLocation.equals(" ") == true) {
					LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
					if (locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
						String message = "Por favor, activa la configuracion de GPS";
						buildAlertMessage(message);
					} else {
						progress.setMessage("Carga");
						progress.show();
						progress.setCancelable(false);
						while (myLocation.equals(" ") == true) {
							myLocation = WayGroup
									.getLatLong(SelectImageActivity.this);
						}
						uploadData();
					}
				} else {
					progress.setMessage("Carga");
					progress.show();
					progress.setCancelable(false);
					uploadData();
				}
			}
		});

	}

	/*
	 * private byte[] readInFile(String path) throws IOException { // TODO
	 * Auto-generated method stub byte[] data = null; File file = new
	 * File(path); InputStream input_stream = new BufferedInputStream(new
	 * FileInputStream( file)); ByteArrayOutputStream buffer = new
	 * ByteArrayOutputStream(); data = new byte[16384]; // 16K int bytes_read;
	 * while ((bytes_read = input_stream.read(data, 0, data.length)) != -1) {
	 * buffer.write(data, 0, bytes_read); } input_stream.close(); return
	 * buffer.toByteArray();
	 * 
	 * }
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		img = (ImageView) findViewById(R.id.imgLoad);

		Uri selectedImageUri;
		if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
			selectedImageUri = data.getData();

			// selectedImagePath = Utils.getPathGallery(this, selectedImageUri);
			selectedImagePath = Utils.getPathnew(this, selectedImageUri);

			addStringTask = new AddStringTask(selectedImagePath, img);
			addStringTask.execute();

		} else if (requestCode == 2 && resultCode == RESULT_OK) {

			// if(data!=null){
			// Bitmap photo = (Bitmap) data.getExtras().get("data");
			// selectedImageUri = data.getData();
			// selectedImagePath=getPath(selectedImageUri);

			if (output != null) {
				selectedImagePath = output.getAbsolutePath();
				if (selectedImagePath != null) {
					addStringTask = new AddStringTask(selectedImagePath, img);
					addStringTask.execute();

				}
			}
		}
	}

	/*
	 * public Bitmap decodeBitmap(Uri selectedImage) throws
	 * FileNotFoundException { BitmapFactory.Options o = new
	 * BitmapFactory.Options(); o.inJustDecodeBounds = true;
	 * BitmapFactory.decodeStream(
	 * getContentResolver().openInputStream(selectedImage), null, o);
	 * 
	 * final int REQUIRED_SIZE = 100;
	 * 
	 * int width_tmp = o.outWidth, height_tmp = o.outHeight; int scale = 1;
	 * while (true) { if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 <
	 * REQUIRED_SIZE) { break; } width_tmp /= 2; height_tmp /= 2; scale *= 2; }
	 * 
	 * BitmapFactory.Options o2 = new BitmapFactory.Options(); o2.inSampleSize =
	 * scale; return BitmapFactory.decodeStream(
	 * getContentResolver().openInputStream(selectedImage), null, o2); }
	 */

	public String getPath(Uri uri) {
		Cursor cursor = getContentResolver().query(uri, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(idx);
		} else
			return null;
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = MediaStore.Images.Media.insertImage(
				inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	}

	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			pwindo.dismiss();

		}

	};

	class AddStringTask extends AsyncTask<Void, String, Bitmap> {
		String imgPath;
		ImageView listImage;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// super.onPreExecute();
			progress.setMessage("Carga");
			progress.show();
			progress.setCancelable(false);
		}

		public AddStringTask(String imgPath, ImageView listImage) {
			// TODO Auto-generated constructor stub
			this.imgPath = imgPath;
			this.listImage = listImage;
		}

		@Override
		protected Bitmap doInBackground(Void... params) {

			Bitmap scaledBitmap = null;

			BitmapFactory.Options options = new BitmapFactory.Options();

			// by setting this field as true, the actual bitmap pixels are not
			// loaded in the memory. Just the bounds are loaded. If
			// you try the use the bitmap here, you will get null.
			options.inJustDecodeBounds = true;
			Bitmap bmp = BitmapFactory.decodeFile(imgPath, options);

			float actualHeight = options.outHeight;
			float actualWidth = options.outWidth;

			// max Height and width values of the compressed image is taken as
			// 816x612

			float maxHeight = 816.0f;
			float maxWidth = 612.0f;
			float imgRatio = actualWidth / actualHeight;
			float maxRatio = maxWidth / maxHeight;

			// width and height values are set maintaining the aspect ratio of
			// the image

			if (actualHeight > maxHeight || actualWidth > maxWidth) {
				if (imgRatio < maxRatio) {
					imgRatio = maxHeight / actualHeight;
					actualWidth = (float) (imgRatio * actualWidth);
					actualHeight = (float) maxHeight;
				} else if (imgRatio > maxRatio) {
					imgRatio = maxWidth / actualWidth;
					actualHeight = (float) (imgRatio * actualHeight);
					actualWidth = (float) maxWidth;
				} else {
					actualHeight = (float) maxHeight;
					actualWidth = (float) maxWidth;

				}
			}

			// setting inSampleSize value allows to load a scaled down version
			// of the original image

			options.inSampleSize = calculateInSampleSize(options, actualWidth,
					actualHeight);

			// inJustDecodeBounds set to false to load the actual bitmap
			options.inJustDecodeBounds = false;

			// this options allow android to claim the bitmap memory if it runs
			// low on memory
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inTempStorage = new byte[16 * 1024];

			try {
				// load the bitmap from its path
				bmp = BitmapFactory.decodeFile(imgPath, options);
			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();

			}

			try {
				// if((int)actualWidth>0 && (int)actualHeight>0){
				scaledBitmap = Bitmap.createBitmap((int) actualWidth,
						(int) actualHeight, Bitmap.Config.ARGB_8888);
				// }

			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			float ratioX = actualWidth / (float) options.outWidth;
			float ratioY = actualHeight / (float) options.outHeight;
			float middleX = actualWidth / 2.0f;
			float middleY = actualHeight / 2.0f;

			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
					middleY - bmp.getHeight() / 2, new Paint(
							Paint.FILTER_BITMAP_FLAG));

			// check the rotation of the image and display it properly
			ExifInterface exif;
			try {
				exif = new ExifInterface(imgPath);

				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 0);
				Log.d("EXIF", "Exif: " + orientation);
				Matrix matrix = new Matrix();
				if (orientation == 6) {
					matrix.postRotate(90);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 3) {
					matrix.postRotate(180);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 8) {
					matrix.postRotate(270);
					Log.d("EXIF", "Exif: " + orientation);
				}
				scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
						scaledBitmap.getWidth(), scaledBitmap.getHeight(),
						matrix, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return scaledBitmap;
		}

		private int calculateInSampleSize(Options options, float reqWidth,
				float reqHeight) {
			// TODO Auto-generated method stub
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				final int heightRatio = Math.round((float) height
						/ (float) reqHeight);
				final int widthRatio = Math.round((float) width
						/ (float) reqWidth);
				inSampleSize = heightRatio < widthRatio ? heightRatio
						: widthRatio;
			}
			final float totalPixels = width * height;
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;
			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}

			return inSampleSize;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			// super.onPostExecute(result);
			Utils.SaveBitmap(result, "img");

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			result.compress(Bitmap.CompressFormat.JPEG, 50, stream);
			image = stream.toByteArray();
			listImage.setImageBitmap(result);
			progress.dismiss();

		}
	}

	public void buildAlertMessage(String message) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(true)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,
							final int id) {

					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
		TextView messageText = (TextView) alert
				.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);

	}

	private void sendNotification() throws ParseException {
		// TODO Auto-generated method stub

		final HashMap<String, Object> pushData = new HashMap<String, Object>();
		//pushData.put("channels", po.getString("deviceID"));
		pushData.put("channels", owner);
		pushData.put("sender",userName);
		pushData.put("message", "Hay una nueva foto de" + " "
				+ userName + ".");
		ParseCloud.callFunctionInBackground("SendPush", pushData, new FunctionCallback<String>() {

			public void done(String success, ParseException e) {
				if (e == null) {
					// ratings is 4.5
					Log.d("MyLog" , "SendPush Success reponse" + success.toString());
				}
				else{
					Log.d("MyLog" , "SendPush Fail reponse" + e.getMessage());
				}
			}
		});

	/*
		final String android_id = "user"
				+ Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DeviceIDs");
		query.whereEqualTo("owner", owner);
		query.whereEqualTo("userType", "1");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(final List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					ParseQuery query1 = ParseInstallation.getQuery();
					for (ParseObject po : objects) {
						if (po.getString("deviceID").compareTo(android_id) != 0) {
							query1.whereEqualTo("channels",
									po.getString("deviceID"));

//
//							ParsePush push = new ParsePush();
//							push.setQuery(query1);
//							push.setMessage("Hay una nueva foto de" + " "
//									+ userName + ".");
//							push.sendInBackground();

							final HashMap<String, Object> pushData = new HashMap<String, Object>();
							//pushData.put("channels", po.getString("deviceID"));
							pushData.put("channels", owner);
							pushData.put("message", "Hay una nueva foto de" + " "
									+ userName + ".");
							ParseCloud.callFunctionInBackground("SendPush", pushData, new FunctionCallback<String>() {

								public void done(String success, ParseException e) {
									if (e == null) {
										// ratings is 4.5
										Log.d("MyLog" , "SendPush Success reponse" + success.toString());
									}
									else{
										Log.d("MyLog" , "SendPush Fail reponse" + e.getMessage());
									}
								}
							});
						}
					}

				}

			};

		});

		*/
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		// super.onWindowFocusChanged(hasFocus);
		// if(myLocation==null||myLocation==" "){
		// myLocation=WayGroup.getLatLong(SelectImageActivity.this);

		// }

	}

	public void uploadDataToParse() {
		ParseFile file = new ParseFile("test.png", image);
		ParseObject imageData = new ParseObject("ImageData");
		imageData.put("description", description.getText().toString());
		imageData.put("title", titleEditText.getText().toString());
		imageData.put("date", date);
		imageData.put("ownername", owner);
		imageData.put("location", myLocation);
		imageData.put("username", userName);
		imageData.put("usertype", userId);
		imageData.put("image", file);
		imageData.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {

					progress.dismiss();
					if (userid.equals("2") == true) {
						saveDataLocaly(true);
					}
					try {
						sendNotification();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					ImageButton btnClosePopup;

					LayoutInflater inflater = (LayoutInflater) SelectImageActivity.this
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View layout = inflater.inflate(R.layout.popup_send,
							(ViewGroup) findViewById(R.id.popup_element));
					pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT, true);
					pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
					btnClosePopup = (ImageButton) layout
							.findViewById(R.id.btn_close_popupShare);
					btnClosePopup
							.setOnClickListener(cancel_button_click_listener);
				} else {
					saveDataLocaly(false);
					if (ni == null) {
						progress.dismiss();
						Toast.makeText(getBaseContext(), "Red no disponible",
								Toast.LENGTH_SHORT).show();
					}
					e.printStackTrace();
				}
			}
		});
	}

	public void saveDataLocaly(boolean isUpload) {
		if (Utils.getImage().getAbsolutePath() != null) {
			ImageInfoUpdate img = new ImageInfoUpdate(userName, titleEditText
					.getText().toString(), description.getText().toString(),
					date, myLocation, Utils.getImage().getAbsolutePath(),
					Integer.toString(userId), isUpload);
			ImageInfoUpdateHelper imgHelper = new ImageInfoUpdateHelper();
			imgHelper.addData(getBaseContext(), img);
			if (progress != null)
				progress.dismiss();
		} else {
			String message = "No se puede guardar la foto.";
			buildAlertMessage(message);
		}
	}

	public void uploadData() {
		Calendar cal = Calendar.getInstance();
		date = new Date(0);
		date = cal.getTime();
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni != null) {
			// Create the ParseFile
			uploadDataToParse();
		} else {
			saveDataLocaly(false);
		}
	}
}
