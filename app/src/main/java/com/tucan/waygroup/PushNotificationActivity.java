package com.tucan.waygroup;

import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.meteosoft.infrastructure.Utils;
import com.parse.ParsePushBroadcastReceiver;
//import com.world.AndroidBadger.ShortcutBadgeException;
//import com.world.AndroidBadger.ShortcutBadger;

import me.leolin.shortcutbadger.ShortcutBadger;

public class PushNotificationActivity extends ParsePushBroadcastReceiver {
	private NotificationManager mNotificationManager;
	String notificationId = "notificationId";
	Notification myNotification;
	CharSequence notificationTitle;
	CharSequence notificationText;
	int badgeCount;

	public static int numMessages = 0;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		String sender;
		SharedPreferences preference = context.getSharedPreferences(
				"MyPREFERENCES", Context.MODE_PRIVATE);
		myNotification = new NotificationCompat.Builder(context).setTicker("")
				.setWhen(System.currentTimeMillis())
				.setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
				.setSmallIcon(R.drawable.ic_launcher)

				.build();

		try {
			String action = intent.getAction();
			String channel = intent.getExtras().getString("com.parse.Channel");
			JSONObject json = new JSONObject(intent.getExtras().getString(
					"com.parse.Data"));

			Log.d("json", "got action " + action + " on channel " + channel
					+ " with:");
			Iterator itr = json.keys();
			while (itr.hasNext()) {
				String key = (String) itr.next();

				if(key.equalsIgnoreCase("send")){
					sender = json.getString(key);
					if (sender.equalsIgnoreCase(Utils.userName)){
						return;
					}
				}
				Log.d("json", "..." + key + " => " + json.getString(key));
				notificationTitle = "WayGroup";
				notificationText = json.getString("alert");
			}
		} catch (JSONException e) {
			Log.d("json", "JSONException: " + e.getMessage());
		}

		Intent notificationIntent = new Intent(context, SplashActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 1,
				notificationIntent, 0);

		// notificationText =user +" "+"uploaded a new photo";
		// notificationTitle = "WayGroup";
		myNotification.setLatestEventInfo(context, notificationTitle,
				notificationText, contentIntent);
		if (isApplicationSentToBackground(context)) {
			numMessages = preference.getInt("notificationId", 0);
			mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(numMessages, myNotification);
			badgeCount = preference.getInt("badgeNo", -1);
			badgeCount = badgeCount + 1;
			if (numMessages > 1000) {
				numMessages = 0;
			} else {
				numMessages = numMessages + 1;
			}

			ShortcutBadger.applyCount(context, badgeCount);
			Editor editor = preference.edit();
			editor.putInt("badgeNo", badgeCount);
			editor.putInt("notificationId", numMessages);
			editor.commit();
			/*try {
				//ShortcutBadger.setBadge(context, badgeCount);
				ShortcutBadger.applyCount(context, badgeCount);
				Editor editor = preference.edit();
				editor.putInt("badgeNo", badgeCount);
				editor.putInt("notificationId", numMessages);
				editor.commit();
			}
			} catch (ShortcutBadgeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		} else {

			Log.v("receive", "subscribedChannels");
			Intent dialogIntent = new Intent(context,
					NotificationRecieverDialogActivity.class);
			dialogIntent.putExtra("notificationMessage", notificationText);
			//dialogIntent.putExtra("sender")
			dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(dialogIntent);
		}

	}

	private boolean isApplicationSentToBackground(Context mcontext) {
		// TODO Auto-generated method stub
		ActivityManager am = (ActivityManager) mcontext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(mcontext.getPackageName())) {
				return true;
			}
		}
		return false;
	}
}
