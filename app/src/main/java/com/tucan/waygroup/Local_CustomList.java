package com.tucan.waygroup;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.meteosoft.waygroup.database.ImageInfo;
import com.meteosoft.waygroup.database.ImageInfoUpdate;

public class Local_CustomList extends ArrayAdapter<ImageInfoUpdate> {
	Context context;
	List<ImageInfoUpdate> users;
	Bitmap myBitmap;
	AddStringTask addStringTask;
	SharedPreferences preference;

	public Local_CustomList(Context context, List<ImageInfoUpdate> users) {
		super(context, 0, users);
		this.context = context;
		this.users = users;

	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		Log.v("MyLog", "Position: " + position);
		ImageInfoUpdate user = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.list_row, parent, false);

		}
		Display display = ((Activity) context).getWindowManager()
				.getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		Log.i("Display Width", "" + width);
		int height = display.getHeight();
		Log.v("height", " " + height);

		height = (int) (0.83 * display.getHeight());
		LinearLayout image_row = (LinearLayout) convertView
				.findViewById(R.id.image_row);
		android.view.ViewGroup.LayoutParams params = image_row
				.getLayoutParams();
		if (params == null) {
			params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
		} else {
			params.height = height;
		}

		image_row.setLayoutParams(params);

		ImageView listImage = (ImageView) convertView
				.findViewById(R.id.listImage);
		ImageButton moreBtnClick=(ImageButton)convertView.findViewById(R.id.moreBtnClick); 
		moreBtnClick.setTag(user);

		final TextView DescriptionText = (TextView) convertView
				.findViewById(R.id.DescriptionText);
		TextView titleTextView = (TextView) convertView
				.findViewById(R.id.titleText);
		TextView LocationText = (TextView) convertView
				.findViewById(R.id.LocationText);
		TextView TimeText = (TextView) convertView.findViewById(R.id.TimeText);
		TextView DateText = (TextView) convertView.findViewById(R.id.DateText);
		preference = context.getSharedPreferences("MyPREFERENCES",
				Context.MODE_PRIVATE);
		String userName = preference.getString("username2", " ");

		DescriptionText.setText(user.Description);
		if (user.getTitle() != null && !user.getTitle().equals("")) {
			titleTextView.setVisibility(View.VISIBLE);
			titleTextView.setText(user.getTitle());
		} else {
			titleTextView.setVisibility(View.GONE);
		}
		LocationText.setText(user.Location);
		final String imgPath = user.ImgPath;

		File imgFile = new File(imgPath);
		if (imgFile.exists()) {
			// listImage.setImageBitmap(bm);

			if (addStringTask != null) {
				addStringTask.cancel(true);
			}
			listImage.setImageResource(R.drawable.place_holder);
			addStringTask = new AddStringTask(imgPath, listImage);
			addStringTask.execute();
		} else {
			listImage.setImageResource(R.drawable.place_holder);
		}
		Date date = new Date();
		date = user.DateTime;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		String month = month_date.format(cal.getTime());
		int Date = cal.get(Calendar.DAY_OF_MONTH);
		SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		String time = sdfs.format(date).toString();
		String dDate = month + " " + Date + " " + year;
		TimeText.setText(time);
		DateText.setText(dDate);
		DescriptionText.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				DescriptionText
						.setMovementMethod(new ScrollingMovementMethod());
				return false;
			}
		});
		listImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final PopupWindow pwindo;

				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.image_show, parent,
						false);

				pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT, true);
				pwindo.showAtLocation(parent, Gravity.CENTER, 0, 0);
				ImageView showImage = (ImageView) layout
						.findViewById(R.id.imageShow);
				ImageButton cross_btn = (ImageButton) layout
						.findViewById(R.id.cross_btn);
				try {
					Bitmap myBitmap = BitmapFactory.decodeFile(imgPath);
					ExifInterface exif = new ExifInterface(imgPath);
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION, 1);
					Log.d("EXIF", "Exif: " + orientation);
					Matrix matrix = new Matrix();
					if (orientation == 6) {
						matrix.postRotate(90);
					} else if (orientation == 3) {
						matrix.postRotate(180);
					} else if (orientation == 8) {
						matrix.postRotate(270);
					}
					myBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
							myBitmap.getWidth(), myBitmap.getHeight(), matrix,
							true);
					showImage.setImageBitmap(myBitmap);
				} catch (Exception e) {

				}

				cross_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pwindo.dismiss();
					}
				});
			}
		});
		
		moreBtnClick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final PopupWindow pwindo;

				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.description_show, parent,
						false);
				ImageInfoUpdate user=(ImageInfoUpdate)v.getTag();
				pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT, true);
				pwindo.showAtLocation(parent, Gravity.CENTER, 0, 0);
				ImageView showImage = (ImageView) layout
						.findViewById(R.id.imageShow);
				ImageButton cross_btn = (ImageButton) layout
						.findViewById(R.id.cross_btn);
				TextView title=(TextView)layout.findViewById(R.id.titleTextView);
				TextView desc=(TextView)layout.findViewById(R.id.description);
				desc.setText(user.Description);
				if (user.getTitle() != null && !user.getTitle().equals("")) {
					title.setVisibility(View.VISIBLE);
					title.setText(user.getTitle());
				} else {
					title.setVisibility(View.GONE);
				}
				try {
					Bitmap myBitmap = BitmapFactory.decodeFile(imgPath);
					ExifInterface exif = new ExifInterface(imgPath);
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION, 1);
					Log.d("EXIF", "Exif: " + orientation);
					Matrix matrix = new Matrix();
					if (orientation == 6) {
						matrix.postRotate(90);
					} else if (orientation == 3) {
						matrix.postRotate(180);
					} else if (orientation == 8) {
						matrix.postRotate(270);
					}
					myBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
							myBitmap.getWidth(), myBitmap.getHeight(), matrix,
							true);
					showImage.setImageBitmap(myBitmap);
				} catch (Exception e) {

				}

				cross_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pwindo.dismiss();
					}
				});
				
			}
		});
		
		

		return convertView;
	}

	class AddStringTask extends AsyncTask<Void, String, Bitmap> {
		String imgPath;
		ImageView listImage;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// super.onPreExecute();

		}

		public AddStringTask(String imgPath, ImageView listImage) {
			// TODO Auto-generated constructor stub
			this.imgPath = imgPath;
			this.listImage = listImage;
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			try {
				myBitmap = BitmapFactory.decodeFile(imgPath);
				ExifInterface exif = new ExifInterface(imgPath);
				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 1);
				Log.d("EXIF", "Exif: " + orientation);
				Matrix matrix = new Matrix();
				if (orientation == 6) {
					matrix.postRotate(90);
				} else if (orientation == 3) {
					matrix.postRotate(180);
				} else if (orientation == 8) {
					matrix.postRotate(270);
				}
				myBitmap = Bitmap
						.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
								myBitmap.getHeight(), matrix, true);

			} catch (Exception e) {

			}
			// TODO Auto-generated method stub
			return myBitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			// super.onPostExecute(result);
			listImage.setImageBitmap(result);
		}
	}

}
