package com.tucan.waygroup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class AlarmRecieverDialogActivity extends Activity {

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_alarm_reciever_dialog);
		
		showAlertDialog();
	}

	public void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	builder.setTitle("Oops");
		builder.setMessage("Verifique su contrase�a");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				// do something when the OK button is clicked
				AlarmRecieverDialogActivity.this.finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		  TextView messageText = (TextView) alert
				    .findViewById(android.R.id.message);
				  messageText.setGravity(Gravity.CENTER);
				 int textViewId = alert.getContext().getResources()
						    .getIdentifier("android:id/alertTitle", null, null);
						  TextView tv = (TextView) alert.findViewById(textViewId);
						  if (tv != null) {
						   tv.setTextColor(getResources().getColor(
						     android.R.color.white));
						   tv.setGravity(Gravity.CENTER);
						  }
				   
	}
	
}
